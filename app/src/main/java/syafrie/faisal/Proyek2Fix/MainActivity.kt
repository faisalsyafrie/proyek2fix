package syafrie.faisal.Proyek2Fix

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.Result
import kotlinx.android.synthetic.main.activity_main.*
import me.dm7.barcodescanner.core.IViewFinder
import me.dm7.barcodescanner.zxing.ZXingScannerView

class MainActivity : AppCompatActivity(), View.OnClickListener {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
		button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button8.setOnClickListener(this)
    }



    override fun onStart() {
        super.onStart()
    }


    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.button4 -> {
                var intent = Intent(this, ScanCustomer::class.java)
                startActivity(intent)
            }
			R.id.button5 -> {
                var intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            }
            R.id.button8 -> {
                var intent= Intent()
                super.finish()
            }
        }
    }
	
	override fun finish(){
        var intent= Intent()
        super.finish()
    }

}