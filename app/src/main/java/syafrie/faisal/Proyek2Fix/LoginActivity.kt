package syafrie.faisal.Proyek2Fix

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.Result
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.login_activity.*
import me.dm7.barcodescanner.core.IViewFinder
import me.dm7.barcodescanner.zxing.ZXingScannerView

class LoginActivity : AppCompatActivity(), View.OnClickListener {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)
		button2.setOnClickListener(this)
        button3.setOnClickListener(this)
    }



    override fun onStart() {
        super.onStart()
    }


    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.button2 -> {
                Toast.makeText(this, "Berhasil Login",Toast.LENGTH_LONG).show()
                var intent = Intent(this, MainAdmin::class.java)
                startActivity(intent)
            }
			R.id.button3 -> {
                var intent= Intent()
                super.finish()
            }
        }
    }
	
	override fun finish(){
        var intent= Intent()
        super.finish()
    }

}