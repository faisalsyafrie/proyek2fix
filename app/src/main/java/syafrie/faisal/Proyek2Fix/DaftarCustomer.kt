package syafrie.faisal.Proyek2Fix

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.daftar_customer.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.reflect.Method
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

import kotlin.collections.HashMap


class DaftarCustomer : AppCompatActivity(), View.OnClickListener {
    lateinit var mediaHelper : MediaHelper
    lateinit var daftarAdapter : AdapterDaftarCustomer
    lateinit var kategoriAdapter : ArrayAdapter<String>
    var daftarCustomer = mutableListOf<HashMap<String,String>>()
    var daftarKategori = mutableListOf<String>()
    val url = "http://192.168.43.66/proyek2/show_data.php"
    val url2 = "http://192.168.43.66/proyek2/get_nama_kategori.php"
    val url3 = "http://192.168.43.66/proyek2/query_upd_del_ins.php"
    val url4 = "http://192.168.43.66/proyek2/upload.php"
    var imStr = ""
	var namafile = ""
	var fileUri = Uri.parse("")
    var pilihKategori =""

    var tahun = 0
    var bulan = 0
    var hari = 0
    var jam = 0
    var menit = 0
    var tanggal : String=""
    var varradio : String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.daftar_customer)
        daftarAdapter = AdapterDaftarCustomer(daftarCustomer, this)
        mediaHelper = MediaHelper(this)
        val cal: Calendar = Calendar.getInstance()
        val outputFormat : DateFormat = SimpleDateFormat("yyyy-MM-dd")
        tanggal = outputFormat.format(cal.getTime())


        button2.setOnClickListener(this)
        button3.setOnClickListener(this)

		
		try{
			val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
			m.invoke(null)
		}catch(e:Exception){
			e.printStackTrace()
		}

    }
	

    override fun onClick(v: View?) {
        when(v?.id){


            R.id.button2->{
                //queryInsertUpdateDelete("insert")
                var intent = Intent(this, GeneratorQRCode::class.java)
                startActivity(intent)
            }
            R.id.button3->{
                var intent= Intent()
                super.finish()
            }

        }
    }
	
//	fun requestPermissions() = runWithPermissions(
//        Manifest.permission.WRITE_EXTERNAL_STORAGE,
//	Manifest.permission.CAMERA){
//		fileUri =mediaHelper.getOutputMediaFileUri()
//		val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//		intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
//		startActivityForResult(intent, mediaHelper.getRcCamera())
//	}
	


    fun queryInsertUpdateDelete(mode:String){
        val request = object : StringRequest(
            Method.POST,url3,
            Response.Listener{response->
                val jsonObject = JSONObject(response)
                val error =jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this, "Operasi berhasil",Toast.LENGTH_LONG).show()

                }else{
                    Toast.makeText(this, "Operasi gagal",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener{ error->
                Toast.makeText(this, "Tidak dapat terhubung ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm= HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())+".jpg"
                when(mode){
                    "insert" -> {
                        hm.put("mode","insert")
                        hm.put("nama",editTextTextPersonName.text.toString())
                        hm.put("telp",editTextPhone.text.toString())
                        hm.put("tanggal",tanggal)

                    }
//                    "update"->{
//                        hm.put("mode","update")
//                        hm.put("judul",edJudul.text.toString())
//                        hm.put("isi",edIsiBerita.text.toString())
//                        hm.put("tanggal",tanggal)
//                        hm.put("image",imStr)
//                        hm.put("file",nmFile)
//                        hm.put("nama_kategori",pilihKategori)
//                    }
//                    "delete"->{
//                        hm.put("mode","delete")
//                        hm.put("judul",edJudul.text.toString())
//
//                    }
                }
                return hm
            }
        }
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)

    }

    override fun onStart() {
        super.onStart()

    }


    override fun finish(){
        var intent= Intent()
        super.finish()
    }
}