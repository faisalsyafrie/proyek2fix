package syafrie.faisal.Proyek2Fix

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore

import android.util.Base64
import android.util.Log

import android.widget.ImageView
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class MediaHelper(context : Context){
	var namaFile=""
	var fileUri = Uri.parse("")
	//val RC_CAMERA = 100
    
	val context = context
    fun getRcGallery(): Int{
        return REQ_CODE_GALLERY
    }

    companion object {
        const val REQ_CODE_GALLERY = 100

    }
	
	fun getMyFileName() : String{
		return this.namaFile
	}
	
	fun getOutputMediaFile() : File?{
		val mediaStorageDir = File(
            Environment.getExternalStoragePublicDirectory(
		Environment.DIRECTORY_DCIM),"appx06")
		if(!mediaStorageDir.exists())
			if(!mediaStorageDir.mkdirs()){
				Log.e("mkdir","Gagal membuat direktori")
			}
		var mediaFile= File(mediaStorageDir.path+ File.separator+
		"${this.namaFile}")
		return mediaFile
	}

//    fun getRcCamera() : Int{
//        return this.RC_CAMERA
//    }
	
	fun getOutputMediaFileUri() : Uri {
		val timeStamp = SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())
		this.namaFile = "DC_${timeStamp}.jpg"
		this.fileUri = Uri.fromFile(getOutputMediaFile())
		return this.fileUri
	}

    fun bitmapToString(bmp : Bitmap): String{
        val outputStream = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.JPEG, 60,outputStream)
        val byteArray = outputStream.toByteArray()
        return Base64.encodeToString(byteArray,Base64.DEFAULT)
    }
    fun getBitmapToString(uri : Uri, imv : ImageView) : String{
        var bmp = MediaStore.Images.Media.getBitmap(
            this.context.contentResolver,uri)
        var dim = 720
        if(bmp.height > bmp.width){
            bmp = Bitmap.createScaledBitmap(bmp,
                (bmp.width*dim).div(bmp.height),dim,true)
        }else{
            bmp = Bitmap.createScaledBitmap(bmp,
                dim, (bmp.height*dim).div(bmp.width),true)
        }
        imv.setImageBitmap(bmp)
        return bitmapToString(bmp)
    }


}