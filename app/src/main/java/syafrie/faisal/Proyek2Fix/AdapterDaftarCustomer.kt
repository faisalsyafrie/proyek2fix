package syafrie.faisal.Proyek2Fix

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterDaftarCustomer(val daftarCustomer: List<HashMap<String,String>>, val mainActivity: DaftarCustomer) :
    RecyclerView.Adapter<AdapterDaftarCustomer.HolderDaftarCustomer>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDaftarCustomer.HolderDaftarCustomer {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_berita,p0,false)
        return HolderDaftarCustomer(v)
    }
    override fun getItemCount(): Int{
        return daftarCustomer.size
    }

    inner class HolderDaftarCustomer(v: View) : RecyclerView.ViewHolder(v){
        val txJudul = v.findViewById<TextView>(R.id.txJudul)
        val txKategori = v.findViewById<TextView>(R.id.txKategori)
        val txTanggal = v.findViewById<TextView>(R.id.txTanggal)

    }

    override fun onBindViewHolder(p0: AdapterDaftarCustomer.HolderDaftarCustomer, p1: Int) {
        val data : HashMap<String, String> = daftarCustomer.get(p1)
        p0.txJudul.setText(data.get("nama"))
        p0.txKategori.setText(data.get("telp"))
        p0.txTanggal.setText(data.get("tanggal"))

//        if(p1.rem(2)== 0) p0.cLayout.setBackgroundColor(
//            Color.rgb(230,245,240))
//        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

//        p0.cLayout.setOnClickListener({
//            val pos = mainActivity.daftarKategori.indexOf(data.get("nama_kategori"))
//            mainActivity.spinKategori.setSelection(pos)
//            mainActivity.edJudul.setText(data.get("judul"))
//            mainActivity.edIsiBerita.setText(data.get("isi"))
//
//            Picasso.get().load(data.get("url")).into(mainActivity.imUpload);
//        })

//        if(!data.get("url").equals(""))
//            Picasso.get().load(data.get("url")).into(p0.photo);


    }
}